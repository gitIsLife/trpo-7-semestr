###### forms.py
### Сюда пиши все формы (в смысле html)
### Пример
class AuthorWriteForm(Form): # Наслденики Form
    name = forms.CharField(max_length=30, required=True)
    biography = forms.CharField(max_length=30)
    birthdate = forms.DateField(required=True, widget=forms.SelectDateWidget)
	
	def form_valid(self, form): # здесь проверяют данные на сложные ограничения
		pass                    # например: password == confirm_password
		
###### models.py
### Модели
class Dreamreal(models.Model): # Наследники Model
   website = models.CharField(max_length = 50)
   mail = models.CharField(max_length = 50)
   name = models.CharField(max_length = 50)
   phonenumber = models.IntegerField()
   online = models.ForeignKey('Online', default = 1)
   
   class Meta:
      db_table = "dreamreal"

###### views.py
### Сюда - все запросы (снова html), принимаемые сервером
@login_required
def version_download(request, version_id): # первый аргумент request, дальше - любые параметры
    conn = db_connection.Connection.get_connection()
    file_path = conn.get_pdf(request.user.id, version_id)
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/force-download")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
            return response
    return Http404()
	
###### Понятно, что эта вещь разрастётся на много классов нооо
class TaskRunner:
	def prepare_task(self, task): # подготовка директории и всякое
		pass
	
	def preprocess_task(self, task): # что-то вроде проверки кодстайла или чего угодно
		pass
	
	def run_task(self, task):
		pass
		
	def prepare_test(self, test): # запихивание в директорию самого теста
		pass
		
	def run_test(self, test):
		pass