insert into AccessLevel(level) values
    ('public'), ('confidential'), ('secret'), ('top secret');

insert into Role(name) values
    ('data manager');

insert into Func(title) values
    ('edit author'), ('edit authority'), ('edit version');

insert into Map_FuncRole(func_id, role_id) values
    (1, 1), (2, 1), (3, 1);