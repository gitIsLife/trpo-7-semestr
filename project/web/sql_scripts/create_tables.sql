drop table Map_FuncUser;
drop table Map_FuncRole;
drop table Map_UserRole;
drop table Map_AutherVersion;
drop table Map_UserVersion;
drop table DocumentLog;
drop table RequestLog;
drop table UserLog;
drop table "User";
drop table Version;
drop table Department;
drop table Func;
drop table Role;
drop table AccessLevel;
drop table Auther;
drop table Document;
drop table DocumentActions;

create table Func(
	 id smallint generated always as identity not null primary key 
	,title nvarchar2(20) unique not null
);

create table Role(
	 id smallint generated always as identity not null primary key
	,name nvarchar2(20) unique not null
);

create table "User"(
	 id int not null primary key
	--,email nvarchar(256) unique not null
	--,login nvarchar(50) unique not null
	--,password_hash char(16) not null
	,access_level smallint not null
);

create table Department(
     id int generated as identity not null primary key
    ,address nvarchar2(256) not null
    ,name nvarchar2(256) unique not null
);

create table AccessLevel(
	 id smallint generated always as identity not null primary key
	,lvl nvarchar2(20) unique not null
);

create table Auther(
	 id int generated as identity not null primary key
	,name nvarchar2(100) not null
	,birthdate date null
	,biography nvarchar2(1000) null
	,constraint auther_unique unique(name, birthdate)
);

create table Document(
	 id int generated as identity not null primary key
	,title nvarchar2(100) not null
	,description nvarchar2(500) null
);

create table Version(
	 id int generated as identity not null primary key
	,release_time timestamp null
	,scan nvarchar2(1000) null
	,document_id int not null
	,security_level smallint not null
	,department_id int not null
);

create table UserLog(
	 id int generated as identity not null primary key
	,change_time timestamp not null
	,master_id int not null
	,slave_id int not null
	,new_access_level smallint not null
);

create table RequestLog(
	 id int generated as identity not null primary key
	,user_id int not null
	,request_time timestamp not null
	,requested_version_id int not null
);

create table DocumentActions(
	 id smallint generated as identity not null primary key
	,title nvarchar2(20) unique not null
);

create table DocumentLog(
	 id int generated as identity not null primary key
	,user_id int not null
	,document_id int not null
	,action smallint not null
	,change_time timestamp not null
);

alter table "User"
	add foreign key(access_level) references AccessLevel(id);

alter table Version
	add foreign key(document_id) references Document(id);
alter table Version
	add foreign key(security_level) references AccessLevel(id);
alter table Version
	add foreign key(department_id) references Department(id);

alter table UserLog
	add foreign key(master_id) references "User"(id);
alter table UserLog
	add foreign key(slave_id) references "User"(id);
alter table UserLog
	add foreign key(new_access_level) references AccessLevel(id);

alter table RequestLog
	add foreign key(user_id) references "User"(id);
alter table RequestLog
	add foreign key(requested_version_id) references Version(id);

alter table DocumentLog
	add foreign key(user_id) references "User"(id);
alter table DocumentLog
	add foreign key(document_id) references Version(id);
alter table DocumentLog
	add foreign key(action) references DocumentActions(id);

create table Map_FuncUser(
	 user_id int not null
	,func_id smallint not null
	,primary key(user_id, func_id)
    ,constraint fk_map_funcuser_user foreign key (user_id) references "User"(id)
    ,constraint fk_map_funcuser_func foreign key (func_id) references Func(id)
);

create table Map_FuncRole(
	 role_id smallint not null
	,func_id smallint not null 
	,primary key(role_id, func_id)
    ,constraint fk_map_funcrole_func foreign key (func_id) references Func(id)
    ,constraint fk_map_funcrole_role foreign key (role_id) references Role(id)
);

create table Map_UserRole(
	 role_id smallint not null
	,user_id int not null
	,primary key(role_id, user_id)
    ,constraint fk_map_userrole_role foreign key (role_id) references Role(id)
    ,constraint fk_map_userrole_user foreign key (user_id) references "User"(id)
);

create table Map_AutherVersion(
	 auther_id int not null
	,version_id int not null
	,primary key(auther_id, version_id)
    ,constraint fk_map_autherversion_auther foreign key (auther_id) references Auther(id)
    ,constraint fk_map_autherversion_version foreign key (version_id) references Version(id)
);

create table Map_UserVersion(
	 user_id int not null
	,version_id int not null
	,access_grantes char(1) not null
	,primary key(user_id, version_id)
    ,constraint fk_map_userversion_user foreign key (user_id) references "User"(id)
    ,constraint fk_map_userversion_version foreign key (version_id) references Version(id)
);