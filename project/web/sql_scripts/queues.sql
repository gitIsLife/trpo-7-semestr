create or replace type request_log_queue as object 
    (user_id_ int
	,request_time_ timestamp
	,requested_version_id_ int);
    
execute dbms_aqadm.create_queue_table('RequestLogQueueTable', 'request_log_queue');

execute dbms_aqadm.create_queue('RequestLogQueue', 'RequestLogQueueTable');

execute dbms_aqadm.start_queue('RequestLogQueue');

-- EXECUTE DBMS_AQADM.DROP_QUEUE ('RequestLogQueue');  
-- 
-- EXECUTE DBMS_AQADM.DROP_QUEUE ('RequestLogQueue');  
-- 
-- EXECUTE DBMS_AQADM.DROP_QUEUE_TABLE ('RequestLogQueueTable');

-- declare
-- 	dequeue_options dbms_aq.dequeue_options_t;
-- 	message_properties_array dbms_aq.message_properties_array_t;
-- 	type messages_arr is varray(1) of request_log_queue;
-- 	msgs messages_arr;
-- 	batch pls_integer := 1;
-- 	msgids dbms_aq.msgid_array_t;
-- 	dequed pls_integer;
-- begin
-- 	message_properties_array := dbms_aq.message_properties_array_t();
-- 	message_properties_array.extend(batch);
-- 	msgids := dbms_aq.msgid_array_t();
-- 	dequeue_options.wait := 1;
-- 	msgs := messages_arr();
-- 	msgs.extend(batch);
-- 	
-- 	dequed := dbms_aq.dequeue_array ('RequestLogQueue', dequeue_options, batch,
-- 		message_properties_array, msgs, msgids);
--     dbms_output.put_line(dequed);
-- 	for i in 1..dequed loop
--         dbms_output.put_line('1');
-- 		insert into RequestLog(user_id, request_time, requested_version_id)
-- 			values(msgs(i).user_id_, msgs(i).request_time_, msgs(i).requested_version_id_);
-- 	end loop;
-- 	commit;
-- end;


declare
	dequeue_options dbms_aq.dequeue_options_t;
	message_properties dbms_aq.message_properties_t;
	msg request_log_queue;
	msgid raw(16);
begin
	dequeue_options.wait := 0.1;
	for var in 1..20 loop
		dbms_aq.dequeue ('RequestLogQueue', dequeue_options, message_properties, msg, msgid);
		insert into RequestLog(user_id, request_time, requested_version_id)
			values(msg.user_id_, msg.request_time_, msg.requested_version_id_);
		commit;
	end loop;
	exception
	when others then null;
end;


create or replace type document_log_queue as object(
	 user_id_ int
	,document_id_ int
	,action_ smallint
	,change_time_ timestamp
);
    
execute dbms_aqadm.create_queue_table('DocumentLogQueueTable', 'document_log_queue');

execute dbms_aqadm.create_queue('DocumentLogQueue', 'DocumentLogQueueTable');

execute dbms_aqadm.start_queue('DocumentLogQueue');

declare
	dequeue_options dbms_aq.dequeue_options_t;
	message_properties dbms_aq.message_properties_t;
	msg document_log_queue;
	msgid raw(16);
begin
	dequeue_options.wait := 0.1;
	for var in 1..20 loop
		dbms_aq.dequeue ('DocumentLogQueue', dequeue_options, message_properties, msg, msgid);
		insert into DocumentLog(user_id, document_id, action, change_time)
			values (msg.user_id_, msg.document_id_, msg.action_, msg.change_time_);
		commit;
	end loop;
	exception
	when others then null;
end;


create or replace type user_log_queue as object(
	 change_time_ timestamp
	,master_id_ int
	,slave_id_ int
	,new_access_level_ smallint
);
    
execute dbms_aqadm.create_queue_table('UserLogQueueTable', 'user_log_queue');

execute dbms_aqadm.create_queue('UserLogQueue', 'UserLogQueueTable');

execute dbms_aqadm.start_queue('UserLogQueue');

declare
	dequeue_options dbms_aq.dequeue_options_t;
	message_properties dbms_aq.message_properties_t;
	msg user_log_queue;
	msgid raw(16);
begin
	dequeue_options.wait := dbms_aq.no_wait;
	for var in 1..20 loop
		dbms_aq.dequeue ('UserLogQueue', dequeue_options, message_properties, msg, msgid);
		insert into UserLog (change_time, master_id, slave_id, new_access_level)
			values (msg.change_time_, msg.master_id_, msg.slave_id_, msg.new_access_level_);
		commit;
	end loop;
	exception
	when others then null;
end;