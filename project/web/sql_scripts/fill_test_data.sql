insert into department(address, name)
values('adr1', 'dept1');
insert into department(address, name)
values('adr2', 'dept2');
insert into department(address, name)
values ('adr3', 'dept3');

insert into auther(name, birthdate, biography)
values ('auth1', TO_DATE('2000/01/01', 'yyyy/mm/dd'), 'bio1');
insert into auther(name, birthdate, biography)
values  ('auth2', TO_DATE('2002/02/02', 'yyyy/mm/dd'), 'bio2');

insert into document(title, description)
values ('doc1', 'descr1');
insert into document(title, description)
values ('doc2', 'descr2');

insert into version(document_id, security_level, department_id)
values (1, 1, 1);
insert into version(document_id, security_level, department_id)
values  (1, 2, 1);
insert into version(document_id, security_level, department_id)
values  (1, 3, 2);
insert into version(document_id, security_level, department_id)
values (2, 2, 2);
insert into version(document_id, security_level, department_id)
values (2, 3, 1);
insert into version(document_id, security_level, department_id)
values (2, 4, 1);

insert into Map_AutherVersion(auther_id, version_id)
values(1, 1);
insert into Map_AutherVersion(auther_id, version_id)
values (1, 2);
insert into Map_AutherVersion(auther_id, version_id)
values (1, 3);
insert into Map_AutherVersion(auther_id, version_id)
values(2, 3);
insert into Map_AutherVersion(auther_id, version_id)
values(2, 1);
insert into Map_AutherVersion(auther_id, version_id)
values(2, 6);

insert into "User"(id, access_level) values(1, 1);
insert into Map_UserRole(user_id, role_id) values(1, 1);