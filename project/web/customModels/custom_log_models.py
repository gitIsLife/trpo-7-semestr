class DocumentAction:
    def __init__(self, id, name):
        self.id = id
        self.name = name


class DocumentLog:
    def __init__(self, id, version_id, action, user_id, change_time):
        self.id = id
        self.user_id = user_id
        self.version_id = version_id
        self.action = action
        self.change_time = change_time


class UserLog:
    def __init__(self, id, user_id, access_id, change_date):
        self.id = id
        self.user_id = user_id
        self.access_id = access_id
        self.change_date = change_date


class RequestLog:
    def __init__(self, id, user_id, requested_version_id, request_time):
        self.id = id
        self.user_id = user_id
        self.version_id = requested_version_id
        self.request_date = request_time
