class User:
    def __init__(self, id, access_id):
        self.id = id
        self.access_id = access_id


class AccessLevel:
    def __init__(self, id, name):
        self.id = id
        self.name = name


class DocumentVersion:
    def __init__(self, id, creation_date, access_id, pdf, department_id, document_id):
        self.id = id
        self.creation_date = creation_date
        self.access_id = access_id
        self.pdf = pdf
        self.department_id = department_id
        self.document_id = document_id


class Department:
    def __init__(self, id, address, name):
        self.id = id
        self.address = address
        self.name = name


class Document:
    def __init__(self, id, name, description=''):
        self.id = id
        self.name = name
        self.description = description


class Author:
    def __init__(self, id, name, biography, birthdate):
        self.id = id
        self.name = name
        self.biography = biography
        self.birthdate = birthdate


class AuthorsVersions:
    def __init__(self, author_id, version_id):
        self.author_id = author_id
        self.version_id = version_id
