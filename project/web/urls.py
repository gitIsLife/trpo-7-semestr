from django.conf.urls import url
from web import views, forms


urlpatterns = [
    url(r'^$', views.index, name='index'),

    url(r'^register/$', forms.RegisterFormView.as_view(), name='register'),
    url(r'^login/$', forms.LoginFormView.as_view(), name='login'),
    url(r'^accounts/login/$', forms.LoginFormView.as_view()),
    url(r'^logout/$', forms.LogoutView.as_view(), name='logout'),

    url(r'^documents/', views.user_documents, name='documents'),
    url(r'^departments/', views.user_departments, name='departments'),
    url(r'^authors/', views.user_authors, name='authors'),
    url(r'^version/read/(?P<version_id>[0-9]+)/$', views.version_read, name='version_read'),
    url(r'^version/write/(?P<version_id>[0-9]+)/$', views.version_write, name='version_write'),
    url(r'^version/add/(?P<document_id>[0-9]+)/$', views.version_add, name='version_add'),
    url(r'^document/read/(?P<document_id>[0-9]+)/$', views.document_read, name='document_read'),
    url(r'^document/write/(?P<document_id>[0-9]+)/$', views.document_write, name='document_write'),
    url(r'^document/add/$', views.document_add, name='document_add'),
    url(r'^department/read/(?P<department_id>[0-9]+)/$', views.department_read, name='department_read'),
    url(r'^department/write/(?P<department_id>[0-9]+)/$', views.department_write, name='department_write'),
    url(r'^department/add/$', views.department_add, name='department_add'),
    url(r'^author/add/$', views.author_add, name='author_add'),
    url(r'^author/read/(?P<author_id>[0-9]+)/$', views.author_read, name='author_read'),
    url(r'^author/write/(?P<author_id>[0-9]+)/$', views.author_write, name='author_write'),

    url(r'^version/remove_author/(?P<version_id>[0-9]+)/(?P<author_id>[0-9]+)/$', views.version_remove_author, name='version_remove_author'),
    url(r'^version/add_author/(?P<version_id>[0-9]+)/$', views.version_add_author, name='version_add_author'),
    url(r'^doc_logs/$', views.all_document_logs, name='doc_logs'),
    url(r'^req_logs/$', views.all_request_logs, name='req_logs'),
    url(r'^version/download/(?P<version_id>[0-9]+)/$', views.version_download, name='version_download'),
]
