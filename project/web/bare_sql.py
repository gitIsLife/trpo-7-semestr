from web.customModels.custom_models import (
                        Document,
                        Department,
                        DocumentVersion,
                        Author,
                        User,
                        AccessLevel
                        )
import datetime
from typing import List


def create_tables() -> None:
    pass


def get_document_version(doc_id: int, version_id: int) -> DocumentVersion:
    pass


def get_all_departments() -> List[Department]:
    return [
        Department(1, 'address1', 'depName1'),
        Department(2, 'address2', 'depName2'),
        Department(3, 'address3', 'depName3')
    ]


def get_all_documents() -> List[Document]:
    return [
        Document(1, 'docName1'),
        Document(2, 'docName2', 'docDesc2')
    ]



def get_version_by_id(user_id, version_id):
    return DocumentVersion(1, datetime.datetime.now(), 5, None, 1, 1)



#def get_access_level_by_id(access_id: int) -> str: try using Version.access_level (returns str)
#    return "For everyone"


def get_department_by_id(department_id: int) -> Department:
    return Department(1, 'address1', 'depName1')


def get_document_by_id(document_id: int) -> Document:
    return Document(1, 'docName1', 'docDesc1')



def get_author_by_id(author_id):
    return Author(1, 'Author1name', 'auth1Bio', datetime.datetime.now())


def get_all_authors():
    return [
        Author(1, 'Author1name', 'auth1Bio', datetime.datetime.now()),
        Author(2, 'Author2name', 'auth2Bio', datetime.datetime.now())
    ]


def get_version_authors(version_id):
    return [
        Author(1, 'Author1name', 'auth1Bio', datetime.datetime.now()),
        Author(2, 'Author2name', 'auth2Bio', datetime.datetime.now())
    ]


def get_document_versions_extended(user_id, document_id):
    # add access_name instead of id
    # add departmant name instead of id
    x = DocumentVersion(1, datetime.datetime.now(), 5, None, 1, 1)
    x.department_name = get_department_by_id(x.department_id).name
    x.access_level_name = get_access_level_by_id(x.access_id)

    y = DocumentVersion(1, datetime.datetime.now(), 5, None, 1, 1)
    y.department_name = get_department_by_id(y.department_id).name
    y.access_level_name = get_access_level_by_id(y.access_id)

    return [
        x,
        y
    ]


# def get_user_by_username(username): use get_user_by_id(request.usr.id)
#     return User(1, "user1login", 'us1@emample.com', 'usr1hash', 5)


def version_add_author(version_id, author_id):
    pass


def version_remove_author(version_id, author_id):
    pass


def update_author(author):
    # only for test
    print(author.id)
    print(author.name)
    print(author.biography)
    print(author.birthdate)


def update_version(version):
    # only for test
    print(version.id)
    print(version.creation_date)
    print(version.access_id)
    print(version.department_id)
    print(version.document_id)


def get_all_access_levels():
    return [
        AccessLevel(1, 'For everyone'),
        AccessLevel(2, 'Secret')
    ]


def get_documents_with_versions(user_id, department_id):
    return [
        {
            'document': Document(1, 'docName1', 'docDesc1'),
            'versions': [
                DocumentVersion(1, datetime.datetime.now(), 5, None, 1, 1),
                DocumentVersion(2, datetime.datetime.now(), 5, None, 1, 1)
            ]
        },
        {
            'document': Document(2, 'docName2', 'docDesc2'),
            'versions': [
                DocumentVersion(3, datetime.datetime.now(), 5, None, 1, 2),
                DocumentVersion(4, datetime.datetime.now(), 5, None, 1, 2)
            ]
        }
    ]


def update_department(user_id, department):
    print(department.id)
    print(department.name)
    print(department.address)


def update_document(user_id, document):
    print(document.id)
    print(document.name)
    print(document.description)


def add_department(user_id, department):
    print(department.id)
    print(department.name)
    print(department.address)


def add_document(user_id, document):
    print(document.id)
    print(document.name)
    print(document.description)


def add_version(user_id, version):
    pass


def add_author(user_id, author):
    pass
