from django.http import HttpResponse, HttpResponseForbidden
from django.template import loader
from .exceptions import ShownException
from .db_connection import Connection
from functools import wraps
from django.contrib.auth.decorators import login_required as login_


def exceptions_wrapper(func):
    def execute(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e: #except ShownException as e
            raise
            template = loader.get_template('error.html')
            return HttpResponse(template.render({'exception': e}))
        except Exception:
            template = loader.get_template('error.html')
            return HttpResponse(template.render({'exception': Exception("Something is broken")}))

    return execute


def has_permission(permission):
    def decor(func):
        @wraps(func)
        def wraped(*args, **kwargs):
            conn = Connection.get_connection()
            if not conn.user_has_permission(permission):
                return HttpResponseForbidden()
            return func(*args, **kwargs)
        return wraped
    return decor
	
def login_required(func):
    @login_
    def wrapped(request, *args, **kwargs):
        conn = Connection.get_connection()
        conn.login(request.user.id)
        return func(request, *args, **kwargs)
    return wrapped