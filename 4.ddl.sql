drop table Map_FuncUser;
drop table Map_FuncRole;
drop table Map_UserRole;
drop table Map_AutherVersion;
drop table Map_UserVersion;
drop table DocumentLog;
drop table RequestLog;
drop table UserLog;
drop table "User";
drop table Version;
drop table Department;
drop table Func;
drop table Role;
drop table AccessLevel;
drop table Auther;
drop table Document;
drop table DocumentActions;

create table Func(
	 id smallint generated always as identity not null primary key 
	,title nvarchar2(20) unique not null
);

create table Role(
	 id smallint generated always as identity not null primary key
	,name nvarchar2(20) unique not null
);

create table "User"(
	 id int not null primary key
	--,email nvarchar(256) unique not null
	--,login nvarchar(50) unique not null
	--,password_hash char(16) not null
	,access_level smallint not null
);

create table Department(
     id int generated as identity not null primary key
    ,address nvarchar2(256) not null
    ,name nvarchar2(256) unique not null
);

create table AccessLevel(
	 id smallint generated always as identity not null primary key
	,lvl nvarchar2(20) unique not null
);

create table Auther(
	 id int generated as identity not null primary key
	,name nvarchar2(100) not null
	,birthdate date null
	,biography nvarchar2(1000) null
	,constraint auther_unique unique(name, birthdate)
);

create table Document(
	 id int generated as identity not null primary key
	,title nvarchar2(100) not null
	,description nvarchar2(500) null
);

create table Version(
	 id int generated as identity not null primary key
	,release_time timestamp null
	,scan nvarchar2(1000) null
	,document_id int not null
	,security_level smallint not null
	,department_id int not null
);

create table UserLog(
	 id int generated as identity not null primary key
	,change_time timestamp not null
	,master_id int not null
	,slave_id int not null
	,new_access_level smallint not null
);

create table RequestLog(
	 id int generated as identity not null primary key
	,user_id int not null
	,request_time timestamp not null
	,requested_version_id int not null
);

create table DocumentActions(
	 id smallint generated as identity not null primary key
	,title nvarchar2(20) unique not null
);

create table DocumentLog(
	 id int generated as identity not null primary key
	,user_id int not null
	,document_id int not null
	,action smallint not null
	,change_time timestamp not null
);

alter table "User"
	add foreign key(access_level) references AccessLevel(id);

alter table Version
	add foreign key(document_id) references Document(id);
alter table Version
	add foreign key(security_level) references AccessLevel(id);
alter table Version
	add foreign key(department_id) references Department(id);

alter table UserLog
	add foreign key(master_id) references "User"(id);
alter table UserLog
	add foreign key(slave_id) references "User"(id);
alter table UserLog
	add foreign key(new_access_level) references AccessLevel(id);

alter table RequestLog
	add foreign key(user_id) references "User"(id);
alter table RequestLog
	add foreign key(requested_version_id) references Version(id);

alter table DocumentLog
	add foreign key(user_id) references "User"(id);
alter table DocumentLog
	add foreign key(document_id) references Version(id);
alter table DocumentLog
	add foreign key(action) references DocumentActions(id);

create table Map_FuncUser(
	 user_id int not null
	,func_id smallint not null
	,primary key(user_id, func_id)
    ,constraint fk_map_funcuser_user foreign key (user_id) references "User"(id)
    ,constraint fk_map_funcuser_func foreign key (func_id) references Func(id)
);

create table Map_FuncRole(
	 role_id smallint not null
	,func_id smallint not null 
	,primary key(role_id, func_id)
    ,constraint fk_map_funcrole_func foreign key (func_id) references Func(id)
    ,constraint fk_map_funcrole_role foreign key (role_id) references Role(id)
);

create table Map_UserRole(
	 role_id smallint not null
	,user_id int not null
	,primary key(role_id, user_id)
    ,constraint fk_map_userrole_role foreign key (role_id) references Role(id)
    ,constraint fk_map_userrole_user foreign key (user_id) references "User"(id)
);

create table Map_AutherVersion(
	 auther_id int not null
	,version_id int not null
	,primary key(auther_id, version_id)
    ,constraint fk_map_autherversion_auther foreign key (auther_id) references Auther(id)
    ,constraint fk_map_autherversion_version foreign key (version_id) references Version(id)
);

create table Map_UserVersion(
	 user_id int not null
	,version_id int not null
	,access_grantes char(1) not null
	,primary key(user_id, version_id)
    ,constraint fk_map_userversion_user foreign key (user_id) references "User"(id)
    ,constraint fk_map_userversion_version foreign key (version_id) references Version(id)
);

insert into AccessLevel(lvl) values ('public');
insert into AccessLevel(lvl) values('confidential');
insert into AccessLevel(lvl) values('secret');
insert into AccessLevel(lvl) values('top secret');

insert into Role(name) values ('data manager');

insert into Func(title) values('edit author');
insert into Func(title) values('edit authority');
insert into Func(title) values('edit version');

insert into Func(title) values('edit department');
insert into Func(title) values('edit document');
insert into Func(title) values('create document');
insert into Func(title) values('create version');

insert into Func(title) values('create author');
insert into Func(title) values('create department');
insert into Func(title) values('change pdf');
insert into Func(title) values('view logs');

insert into Map_FuncRole(func_id, role_id) values(1, 1);
insert into Map_FuncRole(func_id, role_id) values(2, 1);
insert into Map_FuncRole(func_id, role_id) values(3, 1);
insert into Map_FuncRole(func_id, role_id) values(4, 1);
insert into Map_FuncRole(func_id, role_id) values(5, 1);
insert into Map_FuncRole(func_id, role_id) values(6, 1);
insert into Map_FuncRole(func_id, role_id) values(7, 1);
insert into Map_FuncRole(func_id, role_id) values(8, 1);
insert into Map_FuncRole(func_id, role_id) values(9, 1);
insert into Map_FuncRole(func_id, role_id) values(10, 1);
insert into Map_FuncRole(func_id, role_id) values(11, 1);

insert into DocumentActions(title) values('create');
insert into DocumentActions(title) values('edit');
insert into DocumentActions(title) values('add version');

create or replace package app as
    procedure login(user_id int);
    procedure logout;
	procedure log_user_request(version_id int);
	function get_departments return sys_refcursor;
	function get_all_documents return sys_refcursor;
	function get_user_by_id(user_id int) return sys_refcursor;
	function get_version_by_id(version_id int) return sys_refcursor;
	function get_access_level_by_id(access_id int) return sys_refcursor;
	function get_department_by_id(department_id int) return sys_refcursor;
	function get_document_by_id(document_id int) return sys_refcursor;
	function get_author_by_id(author_id int) return sys_refcursor;
	function get_version_authors(version_id_ int) return sys_refcursor;
	function get_document_versions_extended(document_id_ int) return sys_refcursor;
	function get_documents_with_versions(dept_id int) return sys_refcursor;
	procedure register_user(user_id int);
	function user_has_permission(perm varchar2) return Boolean;
	function get_all_access_levels return sys_refcursor;
	function get_all_authors return sys_refcursor;
	procedure update_version(release_t timestamp, lvl int, doc_id int, dept_id int, id_ int);
	procedure update_author(name_ varchar2, bio varchar2, birth timestamp, id_ int);
	procedure version_add_author(ver_id int, auth_id int);
	procedure version_remove_author(ver_id int, auth_id int);
	procedure update_department(addr varchar2, name_ varchar2, id_ int);
	procedure add_department(addr varchar2, name_ varchar2);
	procedure update_document(title_ varchar2, descr varchar2, id_ int);
	procedure add_document(title_ varchar2, descr varchar2);
	procedure add_version(release_t timestamp, doc_id int, lvl int, dept_id int);
	procedure add_author(name_ varchar2, birth timestamp, bio varchar2);
	procedure change_pdf(pdf_ varchar2, ver_id int);
	function get_pdf(ver_id int) return varchar2;
	function get_request_logs return sys_refcursor;
	function get_document_logs return sys_refcursor;
end;

create or replace package body app as
	procedure login(user_id int) as
		lvl int;
	begin
		select count(id) into lvl from "User" where id=user_id;
		if lvl = 1 then
			select access_level into lvl from "User" where id=user_id;
			dbms_session.set_context('app', 'user_id', user_id);
			dbms_session.set_context('app', 'user_level', lvl);
		else
			raise_application_error(-20001, 'User_id not found');
		end if;
	end;
	
	procedure logout as
    begin
		dbms_session.clear_all_context('app');
	end;
	
	procedure log_user_request(version_id int) as
    enqueue_options     dbms_aq.enqueue_options_t;
    message_properties  dbms_aq.message_properties_t;
    message_handle      RAW(16);
    msg request_log_queue;
	begin
        msg := request_log_queue(SYS_CONTEXT('app', 'user_id'), CURRENT_TIMESTAMP(), version_id);
        dbms_aq.enqueue('RequestLogQueue', enqueue_options, message_properties, msg, message_handle);
		commit;
	end;
	
	function get_departments
	return sys_refcursor 
	is
		cur sys_refcursor;
	begin
		open cur for
			select id, address, name from Department;
		return cur;
	end;
	
	function get_all_documents
	return sys_refcursor 
	is
		cur sys_refcursor;
	begin
		open cur for
			select id, title, description from Document;
		return cur;
	end;
	
	function get_user_by_id(user_id int) 
	return sys_refcursor
	is
		cur sys_refcursor;
	begin
		open cur for
			select id, access_level from "User" where id=user_id;
		return cur;
	end;
	
	function get_version_by_id(version_id int) 
	return sys_refcursor
	is
		cur sys_refcursor;
        cnt int;
	begin
		app.log_user_request(version_id);
        select count(m.version_id) into cnt
            from Map_UserVersion m
            where m.user_id = SYS_CONTEXT('app', 'user_id') and m.access_grantes = 1 and m.version_id=version_id;
		open cur for
			select id, release_time, security_level, scan, department_id, document_id
				from Version v
                    where v.id=version_id and (v.security_level <= SYS_CONTEXT('app', 'user_level')
                        or cnt=1);
		return cur;
	end;
	
	function get_access_level_by_id(access_id int)
	return sys_refcursor
	is
		cur sys_refcursor;
	begin
		open cur for
			select lvl from AccessLevel where id=access_id;
		return cur;
	end;
	
	function get_department_by_id(department_id int)
	return sys_refcursor
	is
		cur sys_refcursor;
	begin
		open cur for
			select id, address, name from Department where id=department_id;
		return cur;
	end;
	
	function get_document_by_id(document_id int)
	return sys_refcursor
	is
		cur sys_refcursor;
	begin
		open cur for
			select id, title, description from Document where id=document_id;
		return cur;
	end;
	
	function get_author_by_id(author_id int)
	return sys_refcursor
	is
		cur sys_refcursor;
	begin
		open cur for
			select id, name, biography, birthdate from Auther where id=author_id;
		return cur;
	end;
	
	function get_version_authors(version_id_ int)
	return sys_refcursor
	is
		cur sys_refcursor;
	begin
		open cur for 
			select id, name, biography, birthdate from Auther where id in
            (select auther_id from Map_AutherVersion where Map_AutherVersion.version_id=version_id_);
		return cur;
	end;
	
	function get_document_versions_extended(document_id_ int)
	return sys_refcursor
	is
		cur sys_refcursor;
	begin
        open cur for
            select id, release_time, security_level, scan, department_id, document_id
				from Version
                where document_id=document_id_ and (security_level <= SYS_CONTEXT('app', 'user_level') or
                id in (select version_id from Map_UserVersion 
                    where user_id=SYS_CONTEXT('app', 'user_id') and access_grantes = 1));
		return cur;
	end;
	
	function get_documents_with_versions(dept_id int)
	return sys_refcursor
	is
		cur sys_refcursor;
	begin
        open cur for
            select id, release_time, security_level, scan, department_id, document_id
				from Version left join Map_UserVersion on Version.id=Map_UserVersion.version_id
				where department_id=dept_id and (security_level <= SYS_CONTEXT('app', 'user_level')
				or (user_id=SYS_CONTEXT('app', 'user_id') and access_grantes = 1));
		return cur;
	end;
	
	procedure register_user(user_id int) as
    begin
        insert into "User"(id, access_level) values(user_id, 1);
        commit;
    end;
	
	function user_has_permission(perm varchar2)
	return Boolean
	is
		cnt int;
	begin
        select count(*) into cnt from
			(select func_id from Map_FuncUser where user_id=SYS_CONTEXT('app', 'user_id') union
			select func_id from Map_FuncRole inner join Map_UserRole
			on Map_UserRole.role_id=Map_FuncRole.role_id where user_id=SYS_CONTEXT('app', 'user_id'))
			inner join Func on Func.id=func_id where title=perm;
		return cnt > 0;
	end;
	
	function get_all_access_levels
	return sys_refcursor
	is
		cur sys_refcursor;
	begin
        open cur for
            select id, lvl from AccessLevel;
		return cur;
	end;
	
	function get_all_authors
	return sys_refcursor
	is
		cur sys_refcursor;
	begin
        open cur for
            select id, name, biography, birthdate from Auther;
		return cur;
	end;
	
	procedure update_version(release_t timestamp, lvl int, doc_id int, dept_id int, id_ int) as
	begin
		update Version set release_time=release_t, security_level=lvl, document_id=doc_id, department_id=dept_id
			where Version.id=id_;
		commit;
	end;
	
	procedure update_author(name_ varchar2, bio varchar2, birth timestamp, id_ int) as
	begin
		update Auther set name=name_, biography=bio, birthdate=birth where id=id_;
		commit;
	end;
	
	procedure version_add_author(ver_id int, auth_id int) as
		cnt int;
	begin
		select count(*) into cnt from Map_AutherVersion where version_id=ver_id and auther_id=auth_id;
		if cnt = 0 then
			insert into Map_AutherVersion(version_id, auther_id) values(ver_id, auth_id);
			commit;
		end if;
	end;
	
	procedure version_remove_author(ver_id int, auth_id int) as
	begin
		delete from Map_AutherVersion where version_id=ver_id and auther_id=auth_id;
		commit;
	end;
	
	procedure update_department(addr varchar2, name_ varchar2, id_ int) as
	begin
		update Department set address=addr, name=name_ where id=id_;
		commit;
	end;
	
	procedure add_department(addr varchar2, name_ varchar2) as
	begin
		insert into Department(address, name) values (addr, name_);
		commit;
	end;
	
	procedure update_document(title_ varchar2, descr varchar2, id_ int) as
	begin
		update Document set title=title_, description=descr where id=id_;
		commit;
	end;
	
	procedure add_document(title_ varchar2, descr varchar2) as
	begin
		insert into Document(title, description) values (title_, descr);
		commit;
	end;
	
	procedure add_version(release_t timestamp, doc_id int, lvl int, dept_id int) as
	begin
		insert into Version(release_time, document_id, security_level, department_id)
			values (release_t, doc_id, lvl, dept_id);
		commit;
	end;
	
	procedure add_author(name_ varchar2, birth timestamp, bio varchar2) as
	begin
		insert into Auther(name, birthdate, biography) values(name_, birth, bio);
		commit;
	end;
	
	procedure change_pdf(pdf_ varchar2, ver_id int) as
	begin
		update Version set scan=pdf_ where id=ver_id;
		commit;
	end;
	
	function get_pdf(ver_id int) 
	return varchar2
	is
		sc varchar2(30000);
	begin
		select scan into sc from Version where id=ver_id;
		return sc;
	end;
	
	function get_request_logs
	return sys_refcursor
	is
		cur sys_refcursor;
	begin
		open cur for
			select id, user_id, requested_version_id, request_time from RequestLog;
		return cur;
	end;
	
	function get_document_logs
	return sys_refcursor
	is
		cur sys_refcursor;
	begin
		open cur for
			select DocumentLog.id, document_id, title, user_id, change_time
				from DocumentLog inner join DocumentActions on DocumentActions.id=DocumentLog.action;
		return cur;
	end;
end;

create or replace context app using app;


create or replace trigger user_update after update on "User" for each row
declare
	enqueue_options     dbms_aq.enqueue_options_t;
	message_properties  dbms_aq.message_properties_t;
	message_handle      RAW(16);
	msg user_log_queue;
    master_id int;
    ex Exception;
begin
    master_id := SYS_CONTEXT('app', 'user_id');
    if master_id is null then
        raise ex;
    end if;
	msg := user_log_queue(CURRENT_TIMESTAMP(), SYS_CONTEXT('app', 'user_id'), :new.id, :new.access_level);
	dbms_aq.enqueue('UserLogQueue', enqueue_options, message_properties, msg, message_handle);
end;

create or replace trigger document_create after insert on Document for each row 
declare
	enqueue_options     dbms_aq.enqueue_options_t;
	message_properties  dbms_aq.message_properties_t;
	message_handle      RAW(16);
	msg document_log_queue;
    master_id int;
    ex Exception;
begin
    master_id := SYS_CONTEXT('app', 'user_id');
    if master_id is null then
        raise ex;
    end if;
	msg := document_log_queue(SYS_CONTEXT('app', 'user_id'), :new.id, 1, CURRENT_TIMESTAMP());
	dbms_aq.enqueue('DocumentLogQueue', enqueue_options, message_properties, msg, message_handle);
end;

create or replace trigger document_change after update on Document for each row 
declare
	enqueue_options     dbms_aq.enqueue_options_t;
	message_properties  dbms_aq.message_properties_t;
	message_handle      RAW(16);
	msg document_log_queue;
    master_id int;
    ex Exception;
begin
    master_id := SYS_CONTEXT('app', 'user_id');
    if master_id is null then
        raise ex;
    end if;
	msg := document_log_queue(SYS_CONTEXT('app', 'user_id'), :new.id, 2, CURRENT_TIMESTAMP());
	dbms_aq.enqueue('DocumentLogQueue', enqueue_options, message_properties, msg, message_handle);
end;

create or replace trigger document_new_version after insert on Version for each row 
declare
	enqueue_options     dbms_aq.enqueue_options_t;
	message_properties  dbms_aq.message_properties_t;
	message_handle      RAW(16);
	msg document_log_queue;
    master_id int;
    ex Exception;
begin
    master_id := SYS_CONTEXT('app', 'user_id');
    if master_id is null then
        raise ex;
    end if;
	msg := document_log_queue(SYS_CONTEXT('app', 'user_id'), :new.document_id, 3, CURRENT_TIMESTAMP());
	dbms_aq.enqueue('DocumentLogQueue', enqueue_options, message_properties, msg, message_handle);
end;